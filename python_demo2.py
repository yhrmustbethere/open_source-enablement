#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import time

def timmer(func):
    def wrapper(*args, **kwargs):
        start_time = time.time()
        try:
            res = func(*args, **kwargs)
        except Exception as e:
            print(f"An error occurred: {e}")
            return None
        stop_time = time.time()
        print('run time is %s' % (stop_time - strat_time))
        return res
    return wrapper

@timmer
def foo():
    time.sleep(3)
    print('from foo')



